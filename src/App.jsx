import React from 'react'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import { Helmet } from 'react-helmet'

import { MuiThemeProvider } from '@material-ui/core/styles'
import muiTheme from './utils/muiTheme'

import store from './utils/store'
import { saveState } from './utils/localStorage'
import { Main } from './modules/core'


store.subscribe(() => {
  saveState(store.getState())
})

const App = () => (
  <Provider store={store}>
    <React.Fragment>
      <MuiThemeProvider theme={muiTheme}>
        <React.Fragment>
          <Helmet>
            <link href="https://fonts.googleapis.com/css?family=Share:100,300,400,700|Material+Icons" rel="stylesheet" />
          </Helmet>
          <Router>
            <Route path="/" component={Main} />
          </Router>
        </React.Fragment>
      </MuiThemeProvider>
    </React.Fragment>
  </Provider>
)

export default App
