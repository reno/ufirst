import React from 'react'
import ReactDOM from 'react-dom'

import registerServiceWorker from './registerServiceWorker'
import App from './App'

ReactDOM.render(
  // eslint-disable-next-line
  <App />,
  document.getElementById('root'),
)
registerServiceWorker()
