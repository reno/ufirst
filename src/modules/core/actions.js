import * as types from '../../utils/actionTypes'


// NavIndex
const setNavIndexAction = index => ({
  type: types.SET_NAV_INDEX,
  payload: index,
})

export const setNavIndex = index =>
  dispatch => dispatch(setNavIndexAction(index))


// GraphIndex
const setGraphIndexAction = index => ({
  type: types.SET_GRAPH_INDEX,
  payload: index,
})

export const setGraphIndex = index =>
  dispatch => dispatch(setGraphIndexAction(index))


// Status
const setStatusAction = status => ({
  type: types.SET_STATUS,
  payload: status,
})

export const setStatus = status =>
  dispatch => dispatch(setStatusAction(status))


// Data
const setDataAction = data => ({
  type: types.SET_DATA,
  payload: data,
})

export const setData = data =>
  dispatch => dispatch(setDataAction(data))
