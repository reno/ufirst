import React from 'react'
import Dropzone from 'react-dropzone'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import { CenterWrapper } from '../'

const FileInput = (props) => {
  const {
    classes,
    setData,
    status,
    theme,
  } = props

  return (
    <React.Fragment>
      
      <CenterWrapper>
        <Grid
          className={classes.inputContainer}
          item
          xs={12}
        >
          <Dropzone
            onDrop={file => setData(file[0])}
            multiple={false}
            style={{
              width: '100%',
              height: '100%',
              border: `3px dashed ${theme.palette.text.secondary}`,
              borderRadius: '3px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
            }}
          >
            {status.loading ? (
              <div>
                <div>
                  <CircularProgress size={50} color="secondary" />
                  <Typography>Parsing and aggregating the data</Typography>
                </div>
              </div>
            ) : (
              <Typography>Drop the EPA file here...</Typography>
            )}
          </Dropzone>
        </Grid>
      </CenterWrapper>
      <CenterWrapper>
        <Button
          color="primary"
          href="ftp://ita.ee.lbl.gov/traces/epa-http.txt.Z"
          variant="raised"
        >
          Download EPA File first
        </Button>
      </CenterWrapper>
    </React.Fragment>
  )
}

const styles = () => ({
  inputContainer: {
    height: '300px',
  },
})

export default withStyles(styles)(withTheme()(FileInput))
