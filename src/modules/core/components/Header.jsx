import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import { CenterWrapper } from '../'

const Header = (props) => {
  const {
    classes,
  } = props

  const href = 'https://reno.meyer.id/auth/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiY2ppdmUxeXk2MDA4czA4MjZ5dmE0ZHl6byJ9LCJleHAiOjE1MzI0NzY4MDAsImlhdCI6MTUzMDA5NzAxMn0.r4qqIKoy__wJotZv626WtgGmhGIOXe-RTgkV_sPTFuk'

  return (
    <CenterWrapper marginTop={0}>
      <Grid item xs={12}>
        <Grid container justify="center">
          <figure
            className={classes.logo}
          >
            <img
              alt="Ufirst Logo"
              src="/ufirst.png"
            />
          </figure>
        </Grid>
        <Typography
          className={classes.title}
          variant="display1"
          align="center"
        >
          Homework Assignment by <a href={href} target="_blank">Reno Meyer</a>
        </Typography>
      </Grid>
    </CenterWrapper>
  )
}

const styles = theme => ({
  logo: {
    maxWidth: 200,
    '& img': {
      width: '100%',
    },
  },
  title: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
      marginBottom: 20,
    },
    '& a': {
      color: theme.palette.primary.main,
      '&:hover': {
        color: theme.palette.primary.dark,
      },
    },
  },
})

export default withStyles(styles)(Header)
