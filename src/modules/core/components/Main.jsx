import React from 'react'
import SwipeableViews from 'react-swipeable-views'

import { withStyles } from '@material-ui/core/styles'

import {
  FileInput,
  Header,
} from '../'

import { Graphs } from '../../graphs/'

class Main extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inPlace: props.navIndex,
    }

    this.updateInPlace = this.updateInPlace.bind(this)
  }

  updateInPlace() {
    this.setState({
      inPlace: this.props.navIndex,
    })
  }

  render() {
    const {
      classes,
      navIndex,
    } = this.props

    const { inPlace } = this.state

    return (
      <React.Fragment>
        <Header />
        <SwipeableViews
          axis="y"
          containerStyle={{ height: 600 }}
          index={navIndex}
          onTransitionEnd={this.updateInPlace}
        >
          <div className={classes.slide}>
            <FileInput />
          </div>
          <div className={classes.slide}>
            <Graphs loaded={(inPlace === 1)} />
          </div>
        </SwipeableViews>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.grey[100],
    },
  },
  slide: {
    minHeight: 600,
  },
})

export default withStyles(styles)(Main)
