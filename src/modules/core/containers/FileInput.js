import { connect } from 'react-redux'

import {
  getMethods,
  getRequests,
  getResponseCodes,
  getSizes,
} from '../../../utils/aggregations'
import {
  setData,
  setNavIndex,
  setStatus,
} from '../actions'
import { getStatus } from '../selectors'
import FileInput from '../components/FileInput'

const mapStateToProps = state => ({
  status: getStatus(state),
})

const mapDispatchToProps = dispatch => ({
  setData: (file) => {
    const fileReader = new FileReader()
    fileReader.readAsText(file, 'UTF-8')

    fileReader.onloadstart = () => dispatch(setStatus({
      loading: true,
    }))

    fileReader.onload = (event) => {
      const { result } = event.target

      const data = result.split(/\r?\n/).map((line) => {
        const element = {}
        line.split('"').forEach((nested, index) => {
          const subElements = nested.trim().split(' ')

          if (index === 0) {
            [element.host] = subElements
            if (subElements[1] && subElements[1].length) {
              element.date = subElements[1].substring(1, subElements[1].length).split(':').map(el => parseInt(el, 10))
            }
          }

          if (index === 1) {
            element.request = {
              method: subElements[0],
              url: subElements[1],
              protocol: subElements[2],
              protocol_version: subElements[3],
            }
          }

          if (index === 2) {
            [
              element.response_code,
              element.document_size,
            ] = subElements
          }
        })
        return element
      })

      dispatch(setData({
        methods: getMethods(data),
        requests: getRequests(data),
        responseCodes: getResponseCodes(data),
        sizes: getSizes(data),
      }))
    }

    fileReader.onloadend = () => {
      dispatch(setStatus({
        loading: false,
        success: true,
        error: false,
      }))

      dispatch(setNavIndex(1))
    }

    fileReader.onerror = () => dispatch(setStatus({
      loading: false,
      error: true,
    }))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FileInput)
