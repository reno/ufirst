import { combineReducers } from 'redux'
import * as types from '../../utils/actionTypes'
import { createReducer } from '../../utils/helpers'

const initialState = {
  navIndex: 0,
  graphIndex: 0,
  status: {
    loading: false,
    success: false,
    error: false,
  },
  data: {},
}

// NavIndex
const setNavIndex = (state, payload) => payload

export const navIndex = createReducer(initialState.navIndex, {
  [types.SET_NAV_INDEX]: setNavIndex,
})

// GraphIndex
const setGraphIndex = (state, payload) => payload

export const graphIndex = createReducer(initialState.graphIndex, {
  [types.SET_GRAPH_INDEX]: setGraphIndex,
})

// Status
const setStatus = (state, payload) => ({
  ...state,
  ...payload,
})

export const status = createReducer(initialState.status, {
  [types.SET_STATUS]: setStatus,
})

// Data
const setData = (state, payload) => payload

export const data = createReducer(initialState.data, {
  [types.SET_DATA]: setData,
})

// Default Export
export default combineReducers({
  navIndex,
  graphIndex,
  status,
  data,
})
