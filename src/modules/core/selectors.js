import { createSelector } from 'reselect'

export const getNavIndex = createSelector(
  [
    state => state.core.navIndex,
  ],
  navIndex => navIndex,
)

export const getGraphIndex = createSelector(
  [
    state => state.core.graphIndex,
  ],
  graphIndex => graphIndex,
)

export const getStatus = createSelector(
  [
    state => state.core.status,
  ],
  status => status,
)

export const getData = createSelector(
  [
    state => state.core.data,
  ],
  data => data,
)

export const getRequestsData = createSelector(
  [
    state => state.core.data.requests,
  ],
  requests => requests,
)

export const getMethodsData = createSelector(
  [
    state => state.core.data.methods,
  ],
  methods => methods,
)

export const getResponseCodesData = createSelector(
  [
    state => state.core.data.responseCodes,
  ],
  responseCodes => responseCodes,
)

export const getSizesData = createSelector(
  [
    state => state.core.data.sizes,
  ],
  sizes => sizes,
)
