import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import { CenterWrapper } from '../../core/'

const RequestGraph = (props) => {
  const {
    classes,
    children,
    title,
  } = props

  return (
    <React.Fragment>
      <Paper className={classes.paper}>
        <CenterWrapper marginTop={40}>
          <Typography
            align="center"
            className={classes.title}
            variant="title"
          >
            {title}
          </Typography>
        </CenterWrapper>
        <CenterWrapper marginTop={10}>
          <div className={classes.graphContainer}>
            {children}
          </div>
        </CenterWrapper>
      </Paper>
    </React.Fragment>
  )
}

const styles = theme => ({
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  title: {
    color: theme.palette.text.secondary,
  },
  graphContainer: {
    '& text': {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.fontSize - 2,
    },
  },
})

export default withStyles(styles)(RequestGraph)
