import React from 'react'
import SwipeableViews from 'react-swipeable-views'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import { CenterWrapper } from '../../core/'
import {
  MethodGraph,
  SizeGraph,
  ResponseCodeGraph,
  RequestGraph,
} from '../'

class Graphs extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inPlace: props.graphIndex,
    }

    this.updateInPlace = this.updateInPlace.bind(this)
    this.setGraphIndex = this.setGraphIndex.bind(this)
  }

  setGraphIndex(index) {
    return (() => {
      this.props.setGraphIndex(index)
    })
  }

  updateInPlace() {
    this.setState({
      inPlace: this.props.graphIndex,
    })
  }

  render() {
    const {
      classes,
      graphIndex,
      graphs,
      loaded,
      resetData,
    } = this.props

    const { inPlace } = this.state

    return (
      <React.Fragment>
        <CenterWrapper>
          <Grid container justify="center">
            {graphs && graphs.map((graph, index) => (
              <Button
                className={classes.button}
                color={(index === graphIndex) ? 'primary' : 'default'}
                key={`button-${graph}`}
                onClick={this.setGraphIndex(index)}
                variant="outlined"
              >
                {graph}
              </Button>
            ))}
            <Button
              className={classes.button}
              onClick={resetData}
              variant="raised"
              color="primary"
            >
              Reset the Data
            </Button>
          </Grid>
        </CenterWrapper>
        <CenterWrapper>
          <Grid item xs={12}>
            <SwipeableViews
              axis="x"
              index={graphIndex}
              onTransitionEnd={this.updateInPlace}
            >
              <RequestGraph inPlace={(loaded && inPlace === 0)} />
              <MethodGraph inPlace={(loaded && inPlace === 1)} />
              <ResponseCodeGraph inPlace={(loaded && inPlace === 2)} />
              <SizeGraph inPlace={(loaded && inPlace === 3)} />
            </SwipeableViews>
          </Grid>
        </CenterWrapper>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  button: {
    margin: (theme.spacing.unit * 2),
  },
})

export default withStyles(styles)(Graphs)
