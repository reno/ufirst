import React from 'react'
import { RadialChart } from 'react-vis'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'

import { GraphWrapper } from '../'

const MethodGraph = (props) => {
  const {
    classes,
    data,
    inPlace,
    theme,
  } = props

  return (
    <GraphWrapper title="Distribution of HTTP methods (GET, POST, HEAD,...)">
      <RadialChart
        animation
        className={classes.chart}
        colorRange={[theme.palette.grey[700], theme.palette.primary.main, theme.palette.grey[200], theme.palette.grey[500]]}
        innerRadius={80}
        radius={140}
        data={!data ? [] : data}
        getAngle={d => d.angleArr[inPlace ? 0 : 1]}
        showLabels
        labelsRadiusMultiplier={0.8}
        width={300}
        height={300}
      />
    </GraphWrapper>
  )
}

const styles = () => ({
  chart: {
    '& text': {
      '&:nth-of-type(1)': {
        transform: 'translate(-10px, -40px)',
      },
      '&:nth-of-type(2)': {
        transform: 'translate(-12px, -12px)',
      },
    },
  },
})

export default withStyles(styles)(withTheme()(MethodGraph))
