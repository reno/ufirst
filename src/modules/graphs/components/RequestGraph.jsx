import React from 'react'
import {
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
  XYPlot,
  XAxis,
  YAxis,
} from 'react-vis'

import { withTheme } from '@material-ui/core/styles'

import { GraphWrapper } from '../'

const RequestGraph = (props) => {
  const {
    data,
    inPlace,
    theme,
  } = props

  return (
    <GraphWrapper title="Average (!) requests per minute over the entire time span">
      <XYPlot
        height={300}
        width={600}
        getX={d => d[0]}
        getY={d => (inPlace ? (d[1] / 60) : 0)}
      >
        <HorizontalGridLines />
        <VerticalGridLines />
        <XAxis title="Time" />
        <YAxis title="Requests (average per minute)" />
        <LineSeries
          animation
          color={theme.palette.primary.main}
          data={data}
          style={{
            strokeLinejoin: 'round',
            strokeWidth: 4,
            fill: theme.palette.grey[400],
          }}
        />
      </XYPlot>
    </GraphWrapper>
  )
}

export default withTheme()(RequestGraph)
