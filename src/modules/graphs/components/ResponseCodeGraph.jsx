import React from 'react'
import {
  HorizontalGridLines,
  VerticalGridLines,
  HorizontalBarSeries,
  XYPlot,
  XAxis,
  YAxis,
} from 'react-vis'

import { withTheme } from '@material-ui/core/styles'

import { GraphWrapper } from '../'

const ResponseCodeGraph = (props) => {
  const {
    data,
    inPlace,
    theme,
  } = props

  return (
    <GraphWrapper title="Distribution of HTTP answer codes (200, 404, 302,...)">
      <XYPlot
        height={300}
        width={600}
        getX={d => (inPlace ? d.x : 0)}
        yType="ordinal"
        yDistance={100}
      >
        <HorizontalGridLines />
        <VerticalGridLines />
        <HorizontalBarSeries
          animation
          color={theme.palette.primary.main}
          data={data && data}
        />
        <XAxis title="Amount" />
        <YAxis title="Codes" />
      </XYPlot>
    </GraphWrapper>
  )
}

export default withTheme()(ResponseCodeGraph)
