import React from 'react'
import {
  HorizontalGridLines,
  VerticalGridLines,
  VerticalBarSeries,
  XYPlot,
  XAxis,
  YAxis,
} from 'react-vis'

import { withTheme } from '@material-ui/core/styles'

import { GraphWrapper } from '../'

const SizeGraph = (props) => {
  const {
    data,
    inPlace,
    theme,
  } = props

  return (
    <GraphWrapper title="Distribution of the size of the answer of all requests with code 200 and size < 1000B">
      <XYPlot
        height={300}
        width={600}
        getY={d => d.y[inPlace ? 0 : 1]}
      >
        <HorizontalGridLines />
        <VerticalGridLines />
        <VerticalBarSeries
          animation
          color={theme.palette.primary.main}
          data={data && data}
        />
        <XAxis title="Size (in byte)" />
        <YAxis title="200 codes" />
      </XYPlot>
    </GraphWrapper>
  )
}

export default withTheme()(SizeGraph)
