import { connect } from 'react-redux'

import {
  setData,
  setGraphIndex,
  setNavIndex,
  setStatus,
} from '../../core/actions'
import { getGraphIndex } from '../../core/selectors'
import Graphs from '../components/Graphs'

const mapStateToProps = state => ({
  graphs: ['Requests', 'Methods', 'Codes', 'Sizes'],
  graphIndex: getGraphIndex(state),
})

const mapDispatchToProps = dispatch => ({
  setGraphIndex: index => dispatch(setGraphIndex(index)),
  resetData: () => {
    dispatch(setData({}))
    dispatch(setStatus({
      success: false,
    }))
    dispatch(setNavIndex(0))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Graphs)
