import { connect } from 'react-redux'

import { getMethodsData } from '../../core/selectors'
import MethodGraph from '../components/MethodGraph'

const mapStateToProps = state => ({
  data: getMethodsData(state),
})

export default connect(
  mapStateToProps,
  null,
)(MethodGraph)
