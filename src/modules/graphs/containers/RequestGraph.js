import { connect } from 'react-redux'

import { getRequestsData } from '../../core/selectors'
import RequestGraph from '../components/RequestGraph'

const mapStateToProps = state => ({
  data: getRequestsData(state),
})

export default connect(
  mapStateToProps,
  null,
)(RequestGraph)
