import { connect } from 'react-redux'

import { getResponseCodesData } from '../../core/selectors'
import ResponseCodeGraph from '../components/ResponseCodeGraph'

const mapStateToProps = state => ({
  data: getResponseCodesData(state),
})

export default connect(
  mapStateToProps,
  null,
)(ResponseCodeGraph)
