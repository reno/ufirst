import { connect } from 'react-redux'

import { getSizesData } from '../../core/selectors'
import SizeGraph from '../components/SizeGraph'

const mapStateToProps = state => ({
  data: getSizesData(state),
})

export default connect(
  mapStateToProps,
  null,
)(SizeGraph)
