// Core
export const SET_NAV_INDEX = 'SET_NAV_INDEX'
export const SET_GRAPH_INDEX = 'SET_GRAPH_INDEX'
export const SET_STATUS = 'SET_STATUS'
export const SET_DATA = 'SET_DATA'
