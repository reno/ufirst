import getMethods from './methods'
import getRequests from './requests'
import getResponseCodes from './responseCodes'
import getSizes from './sizes'

export {
  getMethods,
  getRequests,
  getResponseCodes,
  getSizes,
}
