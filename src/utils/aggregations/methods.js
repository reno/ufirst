
const getMethods = (data) => {
  const methodsNames = ['GET', 'POST', 'HEAD']
  const methods = methodsNames.map(name => ({
    label: name,
    angleArr: [0, 1],
  }))

  const getMethodIndex = methodName => methodsNames.indexOf(methodName)

  data.forEach((element) => {
    if (element && element.request && methodsNames.includes(element.request.method)) {
      methods[getMethodIndex(element.request.method)].angleArr[0] += 1
    }
  })
  return methods
}

export default getMethods
