
const getRequests = (data) => {
  const requests = [...Array(24).keys()].map((hour, index) => [index, 0])

  data.forEach((element) => {
    if (element.date) {
      requests[element.date[1]][1] += 1
    }
  })

  return requests
}

export default getRequests
