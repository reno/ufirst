
const getResponseCodes = (data) => {
  const codeObj = data.reduce((groups, element) => {
    const newGroups = { ...groups }
    if (!(element && element.response_code && ((parseInt(element.response_code, 10) > 0) && (parseInt(element.response_code, 10)) < 600))) {
      return newGroups
    }
    if (Object.prototype.hasOwnProperty.call(newGroups, element.response_code)) {
      newGroups[element.response_code] += 1
    } else {
      newGroups[element.response_code] = 1
    }
    return newGroups
  }, {})

  return Object.entries(codeObj).map(el => ({
    x: el[1],
    y: `${el[0]}`,
  }))
}

export default getResponseCodes
