
const getSizes = (data) => {
  const sizesArray = [...Array(10).keys()].map(key => ((key + 1) * 100))
  const sizes = sizesArray.map(key => ({
    x: key,
    y: [0, 0],
  }))

  const getSizeIndex = sizeKey => sizesArray.indexOf(sizeKey)

  data.forEach((element) => {
    if (element && element.response_code && (parseInt(element.response_code, 10) === 200)) {
      const size = (element.document_size && Math.ceil(parseInt(element.document_size, 10) / 100) * 100)
      if (size && (size <= 1000)) {
        sizes[getSizeIndex(size)].y[0] += 1
      }
    }
  })

  return sizes
}

export default getSizes
