import { combineReducers } from 'redux'
import core from '../modules/core/reducers'

export default combineReducers({
  core,
})
