import { createStore, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'
import Reducer from './rootReducer'
import { loadState } from './localStorage'

const persistedState = loadState()

export default createStore(
  Reducer,
  persistedState,
  compose(
    applyMiddleware(
      ReduxThunk,
    ),
    window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f => f),
  ),
)
